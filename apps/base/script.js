//PREVOD MEDZI BIN, OCT, DEC A HEX SUSTAVOU
//branislavblazek.sk, 2018

//todo: dat pred cislo prislusny pocet nul, pre lepsiu citatelnost
var bases = {
	bin: document.getElementById("inpt_bin"),
	oct: document.getElementById("inpt_oct"),
	dec: document.getElementById("inpt_dec"),
	hex: document.getElementById("inpt_hex")};
var arr = ['bin', 'oct', 'dec', 'hex'],
	arr_2 = ['txt', 'bin', 'oct', 'dec', 'hex'];
bases.bin.oninput = function(e){
	var result = convert_from_binary(e.target.value);
	for (var base in arr){
		if (base != 0)
			bases[arr[base]].value = result[arr[base]];
	}
}
bases.oct.oninput = function(e){
	var result = convert_from_octal(e.target.value);
	for (var base in arr){
		if (base != 1)
			bases[arr[base]].value = result[arr[base]];
	}
}
bases.dec.oninput = function(e){
	var result = convert_from_decimal(e.target.value);
	for (var base in arr){
		if (base != 2)
			bases[arr[base]].value = result[arr[base]];
	}
}
bases.hex.oninput = function(e){
	let inpt = e.target.value.toUpperCase();
	var result = convert_from_hexadecimal(inpt);
	for (var base in arr){
		if (base != 3)
			bases[arr[base]].value = result[arr[base]];
	}
}
document.getElementById("inpt_txt_text").oninput = function(e){
	var x = convert_from_text(e.target.value);
	console.table(x);
	for (var base in arr_2){
		if (base != 0)
			document.getElementById("inpt_"+arr_2[base]+"_text").value = x[arr_2[base]];
	}
}
document.getElementById("inpt_bin_text").oninput = function(e){
	var t = e.target.value;
	t = t.split(" ").join("");
	var dec = "";
	//ziskanie dec
	if (t.length % 8 !== 0){
		 dec = complete_number(convert_from_binary(t.slice(0, t.length % 8)).dec, 3, 'left') + " ";}
	for(let i = t.length % 8; i < t.length; i+=8) {
		dec += complete_number(convert_from_binary(t.slice(i, i+8)).dec, 3, 'left') + " ";}
	//ziskanie txt
	t = convert_from_text('', dec);
	console.table(t);
	for (var base in arr_2){
		if (base != 1)
			document.getElementById("inpt_"+arr_2[base]+"_text").value = t[arr_2[base]];
	}
}
document.getElementById("inpt_oct_text").oninput = function(e){
	var t = e.target.value;
	t = t.split(" ").join("");
	var dec = "";
	//ziskanie dec
	if (t.length % 3 !== 0){
		dec = complete_number(convert_from_octal(t.slice(0, t.length % 3)).dec, 3, 'left') + " ";}
	for (let i = t.length % 3; i < t.length; i += 3){
		dec += complete_number(convert_from_octal(t.slice(i, i+3)).dec, 3, 'left') + " ";
	}
	//ziskanie txt
	t = convert_from_text('', dec);
	console.table(t);
	for (var base in arr_2){
		if (base != 2)
			document.getElementById("inpt_"+arr_2[base]+"_text").value = t[arr_2[base]];
	}
}
document.getElementById("inpt_dec_text").oninput = function(e){
	var t = e.target.value;
	var dec = t;
	if (t.indexOf(" ") == -1)
		dec = split_on_spaces(t, 3);
	t = convert_from_text('', dec);
	console.table(t);
	for (var base in arr_2){
		if (base != 3)
			document.getElementById("inpt_"+arr_2[base]+"_text").value = t[arr_2[base]];
	}
}
document.getElementById("inpt_hex_text").oninput = function(e){
	var t = e.target.value.toUpperCase(),
		dec = "";
	if (t.indexOf(" ") == -1){
		t = split_on_spaces(t, 3);
	}
	var arr = t.split(" ");
	for (var item in arr){
		dec += convert_from_hexadecimal(arr[item]).dec + " ";
	}
	t = convert_from_text('', dec);
	console.table(t);
	for (var base in arr_2){
		if (base != 4)
			document.getElementById("inpt_"+arr_2[base]+"_text").value = t[arr_2[base]];
	}
}
function split_on_spaces(text, number){//rozdeli text na pozadovany pocet znakov
	text = String(text);
	//rozdelene textu na casti
	let len = text.length,
		res = "",
		modulo = len % number;
	
	if (len <= number){
		return text;
	} else {
		if (modulo != 0){
			res = text.slice(0, modulo).toString();
		}
		for (let i = modulo;i < len; i += number){
			res += " " + text.slice(i, i+number);
			//aby sa pred vystupom nenechdzala medzera:
			if (i == 0) res = res.substr(1);
		}
		return res;
	}
}//vracia rozdeleny kod
//do ostavajucich miest da nuly
function complete_number(number, length, where){//prida 0 na koniec cisla do poctu
	let nsl = number.toString().length;
	let r = number;
	if (nsl % length == 0){
		return r.toString();
	} else {
		switch(where){
			case 'right':
				for(let i = nsl % length; i < length; i++)
					r += "0";
				break;
			case 'left':
				for (let i = nsl % length; i < length; i++)
					r = "0" + r;
				break;
		}
	}
	return r;
}

function check_letter(code, type){//funkcia na skontrolovanie či v texte nie je zlý znak
	let con = true;
	const len = code.length;
	let array = {
		bin: ['0', '1'],
		oct: ['0', '1', '2', '3', '4', '5', '6', '7'],
		dec: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
		hex: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
	}
	for (let i = 0; i < len; i++){
		let sign_now = code.slice(i, i+1);
		if (array[type].indexOf(sign_now) <= -1){
			con = false;
		}
	}
	return con;
}//vracia true alebo false

function multiplication(code, base_from, hex_to){
	var actual = 0;
	var return_hex;
	
	for (var i = 0; i < code.length; i++){
		if (code.length - i -1 >= 0){
			if (isNaN(Number(code.slice(i, i+1)))){
				let array = ['A', 'B', 'C', 'D', 'E', 'F'];
				let letter = array.indexOf(code.slice(i, i+1)) + 10;
				actual += letter*Math.pow(base_from, code.length - i - 1);
			} else {
				actual += code.slice(i, i+1)*Math.pow(base_from, code.length - i - 1);
			}
		}
	}
	if (hex_to == true && actual > 9){
		var hex_array = ['A', 'B', 'C', 'D', 'E', 'F'];
		actual = hex_array[actual - 10];
	}
	return actual.toString();
}

function division(code, base_to, hex){
	var rest, num = code, ret = "";
	while (true){
		num_2 = num;
		num = Math.floor(num / base_to);
		rest = num_2 % base_to;
		if (hex == true && rest > 9){
			let array = ['A', 'B', 'C', 'D', 'E', 'F'];
			rest = array[rest-10].toString();
		}
		ret = rest + ret;
		if (num == 0) break;
	}
	return ret;
}

function convert_from_text(txt_value, dec_value){
	var dec = "", txt = "";
	if (dec_value == undefined){
		var txt_array = txt_value.split("");
		for (var item in txt_array){
			dec += complete_number(txt_value.charCodeAt(item), 3, 'left') + " ";
		}
	} else {
		dec = dec_value;
	}
	var ret = {bin: "", oct: "", dec: "", hex: "", txt: txt},
		len = txt_value.length;
	var dec_a = dec.split(" ");
	for (var item in dec_a) if (dec_a[item] == "") dec_a.splice(item, 1);
	for (var item in dec_a){
		ret.bin += complete_number(convert_from_decimal(dec_a[item]).bin, 8, 'left') + " ";
		ret.oct += complete_number(convert_from_decimal(dec_a[item]).oct, 3, 'left') + " ";
		ret.hex += convert_from_decimal(dec_a[item]).hex + " ";
		ret.txt += String.fromCharCode(dec_a[item]);
	}
	ret.dec += dec + " ";
	return ret;
}

function convert_from_binary(binary_code){
	const len = binary_code.length;//dlzka stringu
	if (!check_letter(binary_code, "bin")){
		return false;}
	let ret = {oct:"",dec:"",hex:""};
	//ziskanie oct
	if (len % 3 !== 0){
		ret.oct = multiplication(binary_code.slice(0, len % 3), 2);
	}
	for (let i = len % 3; i < len; i += 3){
		ret.oct += "" + multiplication(binary_code.slice(i, i+3), 2);
	}
	//ziskanie dec
	ret.dec = multiplication(binary_code, 2);
	//ziskanie hex
	if (len % 4 !== 0){//pokial je na zaciatku menej ako 4 znakov
		ret.hex = multiplication(binary_code.slice(0,len % 4), 2, true);
	}
	for (let i = len % 4; i < len; i += 4){//zvysne
		ret.hex += multiplication(binary_code.slice(i, i+4), 2, true);
	}
	return ret;
}

function convert_from_octal(octal_code){
	const len = octal_code.length;
	if(!check_letter(octal_code, "oct")){
		return false;}
	let ret = {bin: "", dec: "", hex: ""};
	//ziskanie bin
	ret.bin = division(multiplication(octal_code, 8), 2);
	//ziskanie dec
	ret.dec = multiplication(octal_code, 8);
	//ziskanie hex
	ret.hex = division(multiplication(octal_code, 8), 16, true);
	return ret;
}

function convert_from_decimal(decimal_code){
	if(!check_letter(decimal_code, "dec")){
		return false;}
	//tu sa zapisu vysledky
	let ret = {bin:"",oct:"",hex:""};
	//ziskanie bin
	ret.bin = division(decimal_code, 2);
	//ziskanie oct
	ret.oct = division(decimal_code, 8);
	//ziskanie hex
	ret.hex = division(decimal_code, 16, true);
	return ret;
}

function convert_from_hexadecimal(hex_code){
	const len = hex_code.length;
	if (!check_letter(hex_code, "hex")){
		return false;}
	let ret = {bin:"", oct:"", dec:""};
	//ziskanie dec
	ret.dec = multiplication(hex_code, 16);
	//ziskanie bin
	ret.bin = division(ret.dec, 2);
	//ziskanie oct
	ret.oct = division(ret.dec, 8);
	return ret;
}
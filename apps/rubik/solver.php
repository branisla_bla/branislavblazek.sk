<?php
	class Rubik{
		private $data_array;
		private $center_array = array();
		private $color_array = array();
		private $corner_array = array();
		private $corner_array_center = array();
		private $edge_array = array();

		function __construct($data){
			$this -> data_array = $data;
			$this -> setUp();
			$this -> check();
			$this -> WhiteCross();
			$this -> WhiteUp();
			$this -> MiddleLayer();
			$this -> YellowCross();
			$this -> SwapToEdges();
			$this -> YellowCorners();
			$this -> OrientYellow();
		}

		function __toString(){
			foreach($this -> data_array as $index_side => $side){
				echo "/--$index_side--\\<br>";
				for ($i = 0; $i < 9; $i += 3){
					echo substr($side[$i],0,2) . "  " . substr($side[$i+1],0,2) . "  " . substr($side[$i+2],0,2) . "<br>";
				}
			}
			return "<br>";
		}

		function setUp(){
			foreach($this -> data_array as $index_side => $side){
				foreach($side as $index_pos => $pos){
					$id = substr(ucfirst($index_side), 0, 1) . (intval($index_pos) + 1);
					#ziskanie farieb
					array_push($this -> color_array, $pos);
					#ziskanie stredov
					if ($index_pos == "4"){
						$this -> center_array[$id] = $pos;
					}
				}
			}
			#ziskanie rohov
			foreach($this -> corners_values as $corners){
				$this -> corner_array[] = array(
					$this -> GC($corners[0]),
					$this -> GC($corners[1]),
					$this -> GC($corners[2])
				);
			}
			#ziskanie hran
			foreach($this -> edges_values as $edges){
				$this -> edge_array[] = array(
					$this -> GC($edges[0]),
					$this -> GC($edges[1])
				);
			}
		}

		function check(){
			$per_centers = 0;
			$per_colors = 0;
			$per_corners = 0;
			$per_edges = 0;

			$unique_center = array_unique($this -> center_array);
			$unique_color = array_values(array_unique($this -> color_array));
			$count_unique_corners = 0;
			$count_unique_edges = 0;

			#skontrolovanie stredov
			if (count($unique_center) == 6)
				$per_centers = 1;
			#skontrolovanie farieb
			if (count($unique_color) == 6){
				foreach ($unique_color as $color) {
					$count_color = array_count_values($this -> color_array);
					if ($count_color[$color] == 9){
						$per_colors = 1;
					}
				}
			}
			#skontrolovanie rohov
			foreach($this -> corners_values as $cc){
				$this -> corner_array_center[] = array(
					$this -> GC(substr(ucfirst($cc[0]), 0, 1) . "5"),
					$this -> GC(substr(ucfirst($cc[1]), 0, 1) . "5"),
					$this -> GC(substr(ucfirst($cc[2]), 0, 1) . "5"),
				);
			}
			foreach($this -> corner_array as $abcdef){
				sort($abcdef);
				foreach($this -> corner_array_center as $somevar){
					sort($somevar);
					if ($abcdef == $somevar){
						$count_unique_corners++;
						break;
					}
				}
			}
			if ($count_unique_corners == 8)
				$per_corners = 1;
			#skontrolovanie stran
			foreach($this -> edges_values as $bb){
				$this -> edge_array_center[] = array(
					$this -> GC(substr(ucfirst($bb[0]), 0 ,1) . "5"),
					$this -> GC(substr(ucfirst($bb[1]), 0, 1) . "5")
				);
			}
			foreach($this -> edge_array as $abcdef){
				sort($abcdef);
				foreach($this -> edge_array_center as $somevar){
					sort($somevar);
					if ($abcdef == $somevar){
						$count_unique_edges++;
						break;
					}
				}
			}
			if ($count_unique_edges == 12)
				$per_edges = 1;

			#vsetky podmienky
			if ($per_centers && $per_colors && $per_corners && $per_edges){
				return true;
			} else {
				echo "<table>
					<tr><td>variable</td><td>value</td>
					</tr><tr><td>stredy</td><td>$per_centers</td></tr>
					<tr><td>farby</td><td>$per_colors</td></tr>
					<tr><td>rohy</td><td>$per_corners</td></tr>
					<tr><td>hrany</td><td>$per_edges</td></tr></table>";
				return false;
			}
		}

		function MOVE($direc){
			$clock = [7,4,1,8,5,2,9,6,3];
			$clockwise = [3,6,9,2,5,8,1,4,7];
			$cloned = $this -> data_array;
			switch($direc){
				case 'R':
					$next_s = ['up','back','down','front'];
					$now_s = ['front','up','back','down'];
					$next_n = [[2,6,2,2],[5,3,5,5],[8,0,8,8]];
					$now_n = [[2,2,6,2],[5,5,3,5], [8,8,0,8]];
					foreach($clock as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'R´':
					$direc = substr($direc,0,1);
					$next_s = ['down', 'back', 'up', 'front'];
					$now_s = ['front', 'down', 'back', 'up'];
					$next_n = [[8,0,8,8],[5,3,5,5],[2,6,2,2]];
					$now_n = [[8,8,0,8],[5,5,3,5],[2,2,6,2]];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'L':
					$next_s = ['down', 'back', 'up', 'front'];
					$now_s = ['front', 'down', 'back', 'up'];
					$next_n = [[0,8,0,0], [3,5,3,3], [6,2,6,6]];
					$now_n = [[0,0,8,0], [3,3,5,3], [6,6,2,6]];
					foreach($clock as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for ($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'L´':
					$direc = substr($direc,0,1);
					$next_s = ['up', 'back', 'down', 'front'];
					$now_s = ['front', 'up', 'back', 'down'];
					$next_n = [[0,8,0,0],[3,5,3,3],[6,2,6,6]];
					$now_n = [[0,0,8,0],[3,3,5,3],[6,6,2,6]];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'U':
					$next_s = ['left', 'back', 'right', 'front'];
					$now_s = ['front', 'left', 'back', 'right'];
					$next_n = [[0,0,0,0],[1,1,1,1],[2,2,2,2]];
					$now_n = [[0,0,0,0],[1,1,1,1],[2,2,2,2]];
					foreach($clock as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'U´':
					$direc = substr($direc,0,1);
					$next_s = ['right', 'back', 'left', 'front'];
					$now_s = ['front', 'right', 'back', 'left'];
					$next_n = [[2,2,2,2],[1,1,1,1],[0,0,0,0]];
					$now_n = [[2,2,2,2],[1,1,1,1],[0,0,0,0]];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'D':
					$next_s = ['right', 'back', 'left', 'front'];
					$now_s = ['front', 'right', 'back', 'left'];
					$next_n = [[8,8,8,8],[7,7,7,7],[6,6,6,6]];
					$now_n = [[8,8,8,8],[7,7,7,7],[6,6,6,6]];
					foreach($clock as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'D´':
					$direc = substr($direc,0,1);
					$next_s = ['left', 'back', 'right', 'front'];
					$now_s = ['front', 'left', 'back', 'right'];
					$next_n = [[6,6,6,6],[7,7,7,7],[8,8,8,8]];
					$now_n = [[6,6,6,6],[7,7,7,7],[8,8,8,8]];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'F':
					$next_s = ['right', 'down', 'left', 'up'];
					$now_s = ['up', 'right', 'down', 'left'];
					$next_n = [[6,0,2,8],[3,1,5,7],[0,2,8,6]];
					$now_n = [[8,6,0,2],[7,3,1,5],[6,0,2,8]];
					foreach($clock as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'F´':
					$direc = substr($direc,0,1);
					$next_s = ['left', 'down', 'right', 'up'];
					$now_s = ['up', 'left', 'down', 'right'];
					$next_n = [[8,2,0,6],[5,1,3,7],[2,0,6,8]];
					$now_n = [[6,8,2,0],[7,5,1,3],[8,2,0,6]];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'B':
					$next_s = ['left', 'down', 'right', 'up'];
					$now_s = ['up', 'left', 'down', 'right'];
					$next_n = [[6,8,2,0],[3,7,5, 1],[0,6,8,2]];
					$now_n = [[0,6,8,2],[1,3,7,5],[2,0,6,8]];
					foreach($clock as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'B´':
					$direc = substr($direc, 0,1);
					$next_s = ['right', 'down', 'left', 'up'];
					$now_s = ['up', 'right', 'down', 'left'];
					$next_n = [[2,8,6,0],[5,7,3,1],[8,6,0,2]];
					$now_n = [[0,2,8,6],[1,5,7,3],[2,8,6,0]];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN($direc)][$index] = $this -> GC($direc . $item);
					}
					for($i = 0; $i < 4; $i++){
						$cloned[$next_s[$i]][$next_n[0][$i]] = $this -> data_array[$now_s[$i]][$now_n[0][$i]];
						$cloned[$next_s[$i]][$next_n[1][$i]] = $this -> data_array[$now_s[$i]][$now_n[1][$i]];
						$cloned[$next_s[$i]][$next_n[2][$i]] = $this -> data_array[$now_s[$i]][$now_n[2][$i]];
					}
					$this -> data_array = $cloned;
					break;
				case 'X':
					$next_s = ['left', 'back', 'right', 'front'];
					$now_s = ['front', 'left', 'back', 'right'];
					foreach($clock as $index => $item){
						$cloned[$this -> GN('U')][$index] = $this -> GC('U'. $item);
					}
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('D')][$index] = $this -> GC('D'. $item);
					}
					foreach($next_s as $index => $item){
						$cloned[$item] = $this -> data_array[$now_s[$index]];
					}
					$this -> data_array = $cloned;
					break;
				case 'X´':
					$next_s = ['right', 'back', 'left', 'front'];
					$now_s = ['front', 'right', 'back', 'left'];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('U')][$index] = $this -> GC('U'. $item);
					}
					foreach($clock as $index => $item){
						$cloned[$this -> GN('D')][$index] = $this -> GC('D'. $item);
					}
					foreach($next_s as $index => $item){
						$cloned[$item] = $this -> data_array[$now_s[$index]];
					}
					$this -> data_array = $cloned;
					break;
				case 'Y':
					$next_s = ['up', 'back', 'down', 'front'];
					$now_s = ['front', 'up', 'back', 'down'];
					foreach($clock as $index => $item){
						$cloned[$this -> GN('R')][$index] = $this -> GC('R'. $item);
					}
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('L')][$index] = $this -> GC('L'. $item);
					}
					foreach($next_s as $index => $item){
						$cloned[$item] = $this -> data_array[$now_s[$index]];
					}
					$cloned_back = $cloned;
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('D')][$index] = $this -> GC('D'. $item, $cloned_back);
						$cloned[$this -> GN('B')][$index] = $this -> GC('B'. $item, $cloned_back);
					}
					$cloned_back = $cloned;
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('D')][$index] = $this -> GC('D'. $item, $cloned_back);
						$cloned[$this -> GN('B')][$index] = $this -> GC('B'. $item, $cloned_back);
					}
					$this -> data_array = $cloned;
					break;
				case 'Y´':
					$next_s = ['down', 'back', 'up', 'front'];
					$now_s = ['front', 'down', 'back', 'up'];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('R')][$index] = $this -> GC('R'. $item);
					}
					foreach($clock as $index => $item){
						$cloned[$this -> GN('L')][$index] = $this -> GC('L'. $item);
					}
					foreach($next_s as $index => $item){
						$cloned[$item] = $this -> data_array[$now_s[$index]];
					}
					$cloned_back = $cloned;
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('U')][$index] = $this -> GC('U'. $item, $cloned_back);
						$cloned[$this -> GN('B')][$index] = $this -> GC('B'. $item, $cloned_back);
					}
					$cloned_back = $cloned;
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('U')][$index] = $this -> GC('U'. $item, $cloned_back);
						$cloned[$this -> GN('B')][$index] = $this -> GC('B'. $item, $cloned_back);
					}
					$this -> data_array = $cloned;
					break;
				case 'Z':
					$next_s = ['left', 'down', 'right', 'up'];
					$now_s = ['up', 'left', 'down', 'right'];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('F')][$index] = $this -> GC('F'. $item);
					}
					foreach($clock as $index => $item){
						$cloned[$this -> GN('B')][$index] = $this -> GC('B'. $item);
					}
					foreach($next_s as $index => $item){
						$cloned[$item] = $this -> data_array[$now_s[$index]];
					}
					$cloned_back = $cloned;
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('R')][$index] = $this -> GC('R'. $item, $cloned_back);
						$cloned[$this -> GN('L')][$index] = $this -> GC('L'. $item, $cloned_back);
						$cloned[$this -> GN('U')][$index] = $this -> GC('U'. $item, $cloned_back);
						$cloned[$this -> GN('D')][$index] = $this -> GC('D'. $item, $cloned_back);
					}
					$this -> data_array = $cloned;
					break;
				case 'Z´':
					$next_s = ['right', 'down', 'left', 'up'];
					$now_s = ['up', 'right', 'down', 'left'];
					foreach($clockwise as $index => $item){
						$cloned[$this -> GN('B')][$index] = $this -> GC('B'. $item);
					}
					foreach($clock as $index => $item){
						$cloned[$this -> GN('F')][$index] = $this -> GC('F'. $item);
					}
					foreach($next_s as $index => $item){
						$cloned[$item] = $this -> data_array[$now_s[$index]];
					}
					$cloned_back = $cloned;
					foreach($clock as $index => $item){
						$cloned[$this -> GN('R')][$index] = $this -> GC('R'. $item, $cloned_back);
						$cloned[$this -> GN('L')][$index] = $this -> GC('L'. $item, $cloned_back);
						$cloned[$this -> GN('U')][$index] = $this -> GC('U'. $item, $cloned_back);
						$cloned[$this -> GN('D')][$index] = $this -> GC('D'. $item, $cloned_back);
					}
					$this -> data_array = $cloned;
					break;
			}
		}

		function WhiteCross(){
			#find and move the white center
			$moves = [];
			foreach($this -> data_array as $index_side => $side){
				if ($side[4] == "white"){
					switch($index_side){
						case 'front':
							$moves[] = 'Y';
							break;
						case 'right':
							$moves[] = 'Z';
							break;
						case 'left':
							$moves[] = 'Z´';
							break;
						case 'back':
							$moves[] = 'Y´';
							break;
						case 'up':
							$moves[] = '';
							break;
						case 'down':
							$moves[] = 'Y';
							$moves[] = 'Y';
							break;
					}
					break;
				}
			}
			$this -> do($moves);
			#find if white cross is completed
			$cross = false;
			if ($this -> data_array['up'][1] == 'white' && $this -> data_array['up'][3] == 'white' && $this -> data_array['up'][5] == 'white' && $this -> data_array['up'][7] == 'white'){
				$cross = true;
			}
			if (!$cross){
				$count = [1,3,5,7];
				$total = 0;
				foreach($count as $number){
					if ($this -> data_array['up'][$number] == 'white')
						$total++;
				}
				while ($total < 4){
					$sides = ['front', 'right', 'back', 'left'];
					foreach($sides as $index => $side){
						$moves = [];
						if ($this -> data_array[$side][3] == 'white'){
							$this -> toRightSide($side);
							while ($this -> data_array['up'][3] == 'white'){
								$this -> do(array('U'));
							}
							$this -> do(array('L´'));
							$total++;
						}
						if ($this -> data_array[$side][5] == 'white'){
							$this -> toRightSide($side);
							while($this -> data_array['up'][5] == 'white'){
								$this -> do(array('U´'));
							}
							$this -> do(array('R'));
							$total++;
 						}
 						if ($this -> data_array[$side][7] == 'white'){
 							$this -> toRightSide($side);
 							while($this -> data_array['up'][7] == 'white'){
 								$this -> do(array('U'));
 							}
 							$this -> do(array('F´', 'R´', 'D´', 'R', 'F', 'F'));
 							$total++;
 						}
 						if ($this -> data_array[$side][1] == 'white'){
 							$this -> toRightSide($side);
 							while($this -> data_array['up'][1] == 'white'){
 								$this -> do(array('U'));
 							}
 							$this -> do(array('U´', 'R´', 'U', 'F´'));
 							$total++;
 						}
					}	
					$spodok = [1,3,5,7];
					$opak = [7,3,5,1];
					$pohyb = ['F', 'L', 'R', 'B'];
					while($this -> data_array['down'][1] == 'white' || $this -> data_array['down'][3] == 'white' || $this -> data_array['down'][5] == 'white' || $this -> data_array['down'][7] == 'white'){
						foreach($spodok as $index => $number){
							if ($this -> data_array['down'][$number] == 'white'){
								if ($this -> data_array['up'][$opak[$index]] != 'white'){
									$this -> do(array($pohyb[$index], $pohyb[$index]));
									$total++;
									continue;
								}
								if($this -> data_array['up'][$opak[$index]] == 'white'){
									$this -> do(array('D'));
								}
							}
						}
					}
				}
			}
			$sides = array('front', 'right', 'back', 'left');
			$line1 = array();
			$line2 = array();
			foreach($sides as $side){
				$line1[] = $this -> data_array[$side][1];
				$line2[] = $this -> data_array[$side][4];
			}
			while(($line1[0] != $line2[0] && $line1[1] != $line2[1]) || ($line1[1] != $line2[1] && $line1[2] != $line2[2]) || ($line1[2] != $line2[2] && $line1[3] != $line2[3]) || ($line1[3] != $line2[3] && $line1[0] != $line2[0]) || ($line1[0] != $line2[0] && $line1[2] != $line2[2]) || ($line1[1] != $line2[1] && $line1[3] != $line2[3])){
				$this -> do(array('U´'));
				$sides = array('front', 'right', 'back', 'left');
				$line1 = array();
				$line2 = array();
				foreach($sides as $side){
					$line1[] = $this -> data_array[$side][1];
					$line2[] = $this -> data_array[$side][4];
				}
				if (($line1[0] == $line2[0] && $line1[1] == $line2[1]) && ($line1[1] == $line2[1] && $line1[2] == $line2[2]) && ($line1[2] == $line2[2] && $line1[3] == $line2[3]) && ($line1[3] == $line2[3] && $line1[0] == $line2[0])){
					break;
				} else if (($line1[0] == $line2[3]) && ($line2[0] == $line1[3])){
					$this -> do(array('F', 'F', 'D´', 'L', 'L', 'D', 'F', 'F'));
					break;
				} else if (($line1[0] == $line2[1]) && ($line2[0] == $line1[1])){
					$this -> do(array('X', 'F', 'F', 'D´', 'L', 'L', 'D', 'F', 'F'));
					break;
				} else if(($line1[1] == $line2[2]) && ($line2[1] == $line1[2])){
					$this -> do(array('X', 'X', 'F', 'F', 'D´', 'L', 'L', 'D', 'F', 'F'));
					break;
				} else if(($line1[2] == $line2[3]) && ($line2[2] == $line1[3])){
					$this -> do(array('X´', 'F', 'F', 'D´', 'L', 'L', 'D', 'F', 'F'));
					break;
				} else if (($line1[0] == $line2[0]) && ($line2[2] == $line1[2])){
					$this -> do(array('L', 'L', 'R´', 'R´', 'D', 'D', 'L´', 'L´', 'R', 'R'));
					break;
				} else if (($line1[1] == $line2[1]) && ($line2[3] == $line1[3])){
					$this -> do(array('X', 'L', 'L', 'R´', 'R´', 'D', 'D', 'L´', 'L´', 'R', 'R'));
					break;
				}
			}
		}

		function WhiteUp()
		{
			$white_top = [];
			for($i = 0; $i < 9; $i++)
			{
				$white_top[] = $this -> data_array['up'][$i];
			}
			$how_white = array_count_values($white_top)['white'];
			while($how_white < 9)
			{
				#prebehne spodnu vrstvu
				$white_array = [];
				foreach(['front', 'right', 'back', 'left', 'down'] as $side)
				{
					$white_array[] = $this -> data_array[$side][6];
					$white_array[] = $this -> data_array[$side][8];
				}
				$white_array[] = $this -> data_array['down'][0];
				$white_array[] = $this -> data_array['down'][2];

				while(in_array('white', $white_array))
				{
					$sides = ['front', 'right', 'back', 'left'];
					$down_a = [2,8,7,0];
					foreach($sides as $index => $side)
						{
						$next = $index+1 > 3 ? $sides[0] : $sides[$index+1];
						if ($this -> data_array[$side][8] == 'white' || $this -> data_array[$next][6] == 'white' || $this -> data_array['down'][$down_a[$index]] == 'white')
						{
							$colors = [];
							$wiw; #WhereIsWhite
							if ($this -> data_array[$side][8] == 'white')
							{
								$colors[] = $this -> data_array[$next][6];
								$colors[] = $this -> data_array['down'][$down_a[$index]];
								$wiw = 'front';
							}
							else if ($this -> data_array[$next][6] == 'white')
							{
								$colors[] = $this -> data_array[$side][8];
								$colors[] = $this -> data_array['down'][$down_a[$index]];
								$wiw = 'right';
							} 
							else if ($this -> data_array['down'][$down_a[$index]] == 'white')
							{
								$colors[] = $this -> data_array[$side][8];
								$colors[] = $this -> data_array[$next][6];
								$wiw = 'down';
							}
							foreach($sides as $in => $si_2)
							{
								$ne_2 = $in+1 > 3 ? $sides[0] : $sides[$in+1];
								if (($this -> data_array[$si_2][4] == $colors[0] && $this -> data_array[$ne_2][4] == $colors[1]) || ($this -> data_array[$si_2][4] == $colors[1] && $this -> data_array[$ne_2][4] == $colors[0]))
								{
									switch($index)
									{
										case 1:
											$this -> do(array('D´'));
											break;
										case 2:
											$this -> do(array('D´', 'D´'));
											break;
										case 3:
											$this -> do(array('D'));
											break;
									}
									switch($in)
									{
										case 1: 
											$this -> do(array('X', 'D'));
											break;
										case 2:
											$this -> do(array('X', 'X', 'D', 'D'));
											break;
										case 3:
											$this -> do(array('X´', 'D´'));
											break;
									}
									switch($wiw)
									{
										case 'down':
											$this -> do(array('R´', 'D', 'D', 'R', 'D', 'R´', 'D´', 'R'));
											break;
										case 'right':
											$this -> do(array('R´', 'D´', 'R'));
											break;
										case 'front':
											$this -> do(array('F', 'D', 'F´'));
											break;
									}
									break;
								}
							}
						}
					}
					$white_array = [];
					foreach(['front', 'right', 'back', 'left', 'down'] as $side)
					{
						$white_array[] = $this -> data_array[$side][6];
						$white_array[] = $this -> data_array[$side][8];
					}
					$white_array[] = $this -> data_array['down'][0];
					$white_array[] = $this -> data_array['down'][2];
				}

				#prebehne hornu vrstvu
				$white_array = [];
				foreach(['front', 'right', 'back', 'left'] as $side)
				{
					$white_array[] = $this -> data_array[$side][0];
					$white_array[] = $this -> data_array[$side][2];
				}
				while(in_array('white', $white_array))
				{
					$sides = ['front', 'right', 'back', 'left'];
					foreach($sides as $index => $side)
					{
						$next = $index+1 > 3 ? $sides[0] : $sides[$index+1];
						if ($this -> data_array[$side][2] == 'white' || $this -> data_array[$next][0] == 'white')
						{
							switch($index)
							{
								case 1:
									$this -> do(array('X', 'D'));
									break;
								case 2: 
									$this -> do(array('X', 'X', 'D', 'D'));
									break;
								case 3:
									$this -> do(array('X´', 'D´'));
									break;
							}
							if ($this -> data_array['right'][0] == 'white')
							{
								$this -> do(array('R´', 'D´', 'R'));
							} else if ($this -> data_array['front'][2] == 'white')
							{
								$this -> do(array('F', 'D', 'F´'));
							}
							$white_array = [];
							foreach(['front', 'right', 'back', 'left'] as $side)
							{
								$white_array[] = $this -> data_array[$side][0];
								$white_array[] = $this -> data_array[$side][2];
							}
							break;
						}
					}
				}
				$white_top = [];
				for($i = 0; $i < 9; $i++)
				{
					$white_top[] = $this -> data_array['up'][$i];
				}
				$how_white = array_count_values($white_top)['white'];
			}
		}

		function MiddleLayer()
		{
			$this -> do(array('Y', 'Y'));
			$sides = ['front', 'right', 'back', 'left'];
			$up = [7,5,1,3];
			$without_yellow = 0;
			foreach($sides as $index => $side)
			{
				if ($this -> data_array[$side][1] != 'yellow' && $this -> data_array['up'][$up[$index]] != 'yellow')
				{
					$without_yellow++;
				}
			}
			while($without_yellow >= 1)
			{
				foreach($sides as $index => $side)
				{
					$colors = [];
					$colors[] = $this -> data_array[$side][1];
					$colors[] = $this -> data_array['up'][$up[$index]];
					if ($colors[0] != 'yellow' && $colors[1] != 'yellow')
					{
						foreach($sides as $in => $si)
						{
							if ($colors[0] == $this -> data_array[$si][4] && $index == $in)
							{
								switch($index)
								{
									case 1:
										$this -> do(array('X'));
										break;
									case 2:
										$this -> do(array('X', 'X'));
										break;
									case 3:
										$this -> do(array('X´'));
										break;
								}
								break;
							} else if ($colors[0] == $this -> data_array[$si][4])
							{	
								switch($in)
								{
									case 1:
										$this -> do(array('X', 'U´'));
										break;
									case 2:
										$this -> do(array('X', 'X', 'U´', 'U´'));
										break;
									case 3:
										$this -> do(array('X´', 'U'));
										break;
								}
								switch($index)
								{
									case 1:
										$this -> do(array('U'));
										break;
									case 2:
										$this -> do(array('U', 'U'));
										break;
									case 3:
										$this -> do(array('U´'));
										break;
								}
							}
						}
						if ($this -> data_array['up'][7] == $this -> data_array['right'][4])
						{
							$this -> do(array('U',  'R',  'U´', 'R´', 'U´', 'F´', 'U', 'F'));
						} else if ($this -> data_array['up'][7] == $this -> data_array['left'][4])
						{
							$this -> do(array('U´', 'L´', 'U', 'L', 'U', 'F', 'U´', 'F´'));
						}
					}
				}
				$without_yellow = 0;
				foreach($sides as $index => $side)
				{
					if ($this -> data_array[$side][1] != 'yellow' && $this -> data_array['up'][$up[$index]] != 'yellow')
					{
						$without_yellow++;
					}
				}
			}

			/*$full = 0;#ako poriesit chybajuce tahy
			foreach($sides as $side)
			{
				if ($this -> data_array[$side][3] == $this -> data_array[$side][4] && $this -> data_array[$side][4] == $this -> data_array[$side][5])
				{
					$full++;
				}
			}*/
			foreach($sides as $index => $side)
			{	
				$base = $this -> data_array[$side][4];
				if ($base != $this -> data_array[$side][3] || $base != $this -> data_array[$side][5])
				{
							$next = $index + 1 > 3 ? 0 : $index + 1;
							$prev = $index - 1 < 0 ? 3 : $index - 1;
							if ($this -> data_array[$side][4] == $this -> data_array[$sides[$next]][3])
							{
								switch($index)
								{
									case 1:
										$this -> do(array('X'));
										break;
									case 2:
										$this -> do(array('X', 'X'));
										break;
									case 3:
										$this -> do(array('X´'));
										break;
								}
								echo 'peklo:<br>';
								$this -> do(array('U','R','U´','R´','U´','F´','U','F','U´','R','U´','R´','U´','F´','U','F'));
							}
				}
			}
		}

		function YellowCross()
		{
			while($this -> data_array['up'][1] != 'yellow' || $this -> data_array['up'][3] != 'yellow' || $this -> data_array['up'][5] != 'yellow' || $this -> data_array['up'][7] != 'yellow')
			{
				#skontrolovanie zltej bodky
				if ($this -> data_array['up'][1] != 'yellow' && $this -> data_array['up'][3] != 'yellow' && $this -> data_array['up'][5] != 'yellow' && $this -> data_array['up'][7] != 'yellow') 
				{
					echo 'bodka';
					$this -> do(array('F', 'R', 'U', 'R´', 'U´', 'F´'));
				}
				#skontrolovanie I
				if ($this -> data_array['up'][3] == 'yellow' && $this -> data_array['up'][4] == 'yellow' && $this -> data_array['up'][5] == 'yellow')
				{
					echo 'I';
					$this -> do(array('F', 'R', 'U', 'R´', 'U´', 'F´'));
				} else if ($this -> data_array['up'][1] == 'yellow' && $this -> data_array['up'][4] == 'yellow' && $this -> data_array['up'][7] == 'yellow')
				{
					$this -> do(array('U', 'F', 'R', 'U', 'R´', 'U´', 'F´'));
				}
				#skontrolovanie L
				$up = [7,5,1,3];
				$sides = ['front', 'right', 'back', 'left'];
				foreach($sides as $index => $side)
				{
					$next = $index + 1 > 3 ? 0 : $index + 1;
					$prev = $index - 1 < 0 ? 3 : $index - 1;
					$opos = $index + 2 > 3 ? $index - 2 : $index + 2;
					if ($this -> data_array['up'][$up[$index]] == 'yellow' && $this -> data_array['up'][$up[$next]] == 'yellow' && $this -> data_array['up'][$up[$prev]] != 'yellow' && $this -> data_array['up'][$up[$opos]] != 'yellow')
					{	
						switch($index)
						{
							case 1: 
								$this -> do(array('U'));
								break;
							case 2:
								$this -> do(array('U', 'U'));
								break;
							case 3:
								$this -> do(array('U´'));
						}
						$this -> do(array('Z´', 'B', 'R', 'U', 'R´', 'U´', 'B´', 'Z'));
						break;
					}
				}
			}
		}

		function SwapToEdges()
		{
			$sides = ['front', 'right', 'back', 'left'];
			$correct = 0;
			while($correct < 2)
			{
				$correct = 0;
				foreach($sides as $index => $side)
				{
					$center = $this -> data_array[$side][4];
					$up = $this -> data_array[$side][1];
					if ($center == $up)
					{
						$correct++;
					}
				}
				if ($correct < 2)
				{
					$this -> do(array('U'));
				}
			}
			foreach($sides as $index => $side)
			{
				$prev = $index - 1 < 0 ? 3 : $index - 1;
				$next = $index + 1 > 3 ? 0 : $index + 1;
				$opos = $index + 2 > 3 ? $index - 2 : $index + 2;
				if (($this -> data_array[$side][1] != $this -> data_array[$side][4] && $this -> data_array[$sides[$next]][1] != $this -> data_array[$sides[$next]][4]))
				{	
					switch($index)
					{
						case 1: 
							$this -> do(array('X', 'X'));
							break;
						case 2:
							$this -> do(array('X´'));
							break;
						case 3:
							$this -> do(array('X'));
							break;
					}
					$this -> do(array('R', 'U', 'R´', 'U', 'R', 'U', 'U', 'R´', 'U'));
					break;
				}
				if (($this -> data_array[$side][1] != $this -> data_array[$side][4] && $this -> data_array[$sides[$opos]][1] != $this -> data_array[$sides[$opos]][4]))
				{
					if ($index == 0)
					{
						$this -> do(array('X´'));
					}
					$this -> do(array('U', 'R', 'U', 'R´', 'U', 'R', 'U', 'U', 'R´', 'U', 'X', 'X', 'R', 'U', 'R´', 'U', 'R', 'U', 'U', 'R´', 'U'));
					break;
				}
			}
		}

		function YellowCorners()
		{
			$sides = ['front', 'right', 'back', 'left'];
			$up = [8,2,0,6];
			#pocita pocet spravnych rohov
			$corr = 0;
			foreach($sides as $index => $side)
			{
				$next = $sides[$index + 1 > 3 ? 0 : $index + 1];
				$colors = [];
				$colors[] = $this -> data_array[$side][2];
				$colors[] = $this -> data_array[$next][0];
				$colors[] = $this -> data_array['up'][$up[$index]];
				foreach($colors as $color)
				{
					if ($color == $this -> data_array[$side][4] || $color == $this -> data_array[$next][4] || $color == $this -> data_array['up'][4])
					{
						$corr++;
					}
				}
			}
			#</>
			while($corr < 12)
			{
				foreach($sides as $index => $side)
				{
					$next = $sides[$index + 1 > 3 ? 0 : $index + 1];
					$colors = [];
					$colors[] = $this -> data_array[$side][2];
					$colors[] = $this -> data_array[$next][0];
					$colors[] = $this -> data_array['up'][$up[$index]];
					$corr = 0;
					foreach($colors as $color)
					{
						if ($color == $this -> data_array[$side][4] || $color == $this -> data_array[$next][4] || $color == $this -> data_array['up'][4])
						{
							$corr++;
						}
					}
					if ($corr == 3 || $index == 3)
					{
						switch($index)
						{
							case 1:
								$this -> do(array('X'));
								break;
							case 2:
								$this -> do(array('X', 'X'));
								break;
							case 3:
								$this -> do(array('X´'));
								break;
						}
						$this -> do(array('U', 'R', 'U´', 'L´', 'U', 'R´', 'U´', 'L'));
						break;
					}
				}
				#<pocita pocet spravnych rohov>
				$corr = 0;
				foreach($sides as $index => $side)
				{
					$next = $sides[$index + 1 > 3 ? 0 : $index + 1];
					$colors = [];
					$colors[] = $this -> data_array[$side][2];
					$colors[] = $this -> data_array[$next][0];
					$colors[] = $this -> data_array['up'][$up[$index]];
					foreach($colors as $color)
					{
						if ($color == $this -> data_array[$side][4] || $color == $this -> data_array[$next][4] || $color == $this -> data_array['up'][4])
						{
							$corr++;
						}
					}
				}
				#</>
			}
		}

		function OrientYellow()
		{
			#kontrola
			$sides = ['front', 'right', 'back', 'left'];
			$up = [8,2,0,6];
			$corr = 0;
			foreach($sides as $index => $side)
			{
				$next = $sides[$index + 1 > 3 ? 0 : $index + 1];
				$colors = [];
				$colors[] = $this -> data_array[$side][2];
				$colors[] = $this -> data_array[$next][0];
				$colors[] = $this -> data_array['up'][$up[$index]];					
				if ($colors[0] == $this -> data_array[$side][4] && $colors[1] == $this ->data_array[$next][4] && $colors[2] == $this -> data_array['up'][4])
				{
					$corr++;
				}
			}
			#</>
			$x = 0;
			while ($corr < 4)
			{	
				while($this -> data_array['front'][2] != $this -> data_array['front'][1] || $this -> data_array['right'][0] != $this -> data_array['right'][1] || $this -> data_array['up'][8] != $this -> data_array['up'][4])
				{
					$this -> do(array('R´', 'D´', 'R', 'D'));
				}
				#kontrola
				$sides = ['front', 'right', 'back', 'left'];
				$up = [8,2,0,6];
				$corr = 0;
				foreach($sides as $index => $side)
				{
					$next = $sides[$index + 1 > 3 ? 0 : $index + 1];
					$colors = [];
					$colors[] = $this -> data_array[$side][2];
					$colors[] = $this -> data_array[$next][0];
					$colors[] = $this -> data_array['up'][$up[$index]];					
					if ($colors[0] == $this -> data_array[$side][1] && $colors[1] == $this ->data_array[$next][1] && $colors[2] == $this -> data_array['up'][4])
					{
						$corr++;
					}
				}
				#</>
				$this -> do(array('U'));
				$x = $x + 1;
			}
		}

		function do($moves){
			foreach($moves as $move){
				echo "$move<br>";
				$this -> MOVE($move);
			}
		}

		function GC($coor, $arr = null){
			$side = substr($coor, 0, 1);
			$number = substr($coor, 1);
			$side_list = array(
				"F" => "front",
				"L" => "left",
			 	"R" => "right",
			 	"B" => "back",
			 	"U" => "up",
			 	"D" => "down"
			);
			if ($arr == null)
				return ($this -> data_array[$side_list[$side]][$number-1]);
			else 
				return ($arr[$side_list[$side]][$number-1]);
		}

		function GN($let){
			$side_list = array(
				"F" => "front",
				"L" => "left",
			 	"R" => "right",
			 	"B" => "back",
			 	"U" => "up",
			 	"D" => "down"
			);
			return $side_list[$let];
		}
		function toRightSide($side){
			$moves = array();
			if ($side == 'right'){
				$moves[] = 'X';
			} else if($side == 'back'){
				$moves[] = 'X';
				$moves[] = 'X';
			} else if($side == 'left'){
				$moves[] = 'X´';
			}
			$this -> do($moves);
			$moves = [];
		}

		private $corners_values = array(
			array("U1", "L1", "B3"),
			array("U3", "B1", "R3"),
			array("U7", "F1", "L3"),
			array("U9", "R1", "F3"),
			array("D1", "L9", "F7"),
			array("D3", "F9", "R7"),
			array("D7", "B9", "L7"),
			array("D9", "R9", "B7")
		);
		private $edges_values = array(
			array("F6", "R4"),
			array("R6", "B4"),
			array("B6", "L4"),
			array("L6", "F4"),
			array("F2", "U8"),
			array("R2", "U6"),
			array("B2", "U2"),
			array("L2", "U4"),
			array("F8", "D2"),
			array("L8", "D4"),
			array("R8", "D6"),
			array("B8", "D8"),
		);
	}

	$solver = new Rubik($data);
	echo $solver;
?>